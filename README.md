# sample-helm-cd

Sample GitOps project demonstrating helm integration with Reliza Hub for subsequent consumption by GitOps engine - such as ArgoCD.

## Project structure and assumptions

1. helmchart directory - contains very minimal helm chart emulating real helm chart used
2. Assumes that component projects already have their proper CIs connected to Reliza Hub using actionable script demonstrated [here](https://raw.githubusercontent.com/relizaio/reliza-hub-integrations/master/GitLab/standard-docker-registry/.gitlab-ci.yml).
For this tutorial we will assume that previously created project on Reliza Hub is called **Backend Deployment**.
3. Keep all helm values file in the root of your helm chart directory and use a meaningful naming convention for different environments. I.e., this repository uses *values_test.yaml* for test environment and *values_prod.yaml* for production environment. You may keep your base value files as *values.yaml* unmapped to any environments - this would not affect the CD script execution.

## Set up on Reliza Hub and GitLab sides

1. Make sure your deployment repository (in this sample case it is **this** repository) is created on GitLab. I.e., fork this project into your own repository (or clone and switch origin). Such depository must include .gitlab-ci.yml and at least minimal structure of **this** repository.
2. In Reliza Hub, create a new project referencing this repository, i.e. such project can be named "GitLab CI Deployment". Choose your desired versioning schema for this project, i.e. SemVer or Ubuntu CalVer would be relevant.
3. Create API Key and API Id in Reliza Hub for this project.
4. Create environment variables in GitLab CI/CD for the project for the key and id above, named **RELIZA_PROJECT_API_ID** and **RELIZA_PROJECT_API_KEY**.
5. In Reliza Hub, go to Settings -> click on the Lock icon where it says **Set Organization-wide Project and Product Read API Key:** to generate organization wide read api id and key on Reliza Hub.
6. Create environment variables in GitLab CI/CD for the organization key and id above, named **RELIZA_ORGANIZATION_API_ID** and **RELIZA_ORGANIZATION_API_KEY**.
7. In GitLab, navigate to User Settings -> [Access Tokens](https://gitlab.com/profile/personal_access_tokens). Create a personal token with **write_repository** scope. In GitLab CI/CD, create an environment variable for this token named **GIT_PUSH_TOKEN**. This token would be used to push updated from CI runs to repository.
8. In GitLab, in your project, navigate to CI/CD Schedule and create a new schedule, you may set it to run once a month. Once the Schedule is created, click on the **Edit (Pencil) icon** and note schedule id, which shows up in the URI above, i.e. https://gitlab.com/project_namespace/project_name/-/pipeline_schedules/**12345**/edit. For the link above the Schedule id you are looking for is **12345**.
9. In GitLab, navigate to User Settings -> [Access Tokens](https://gitlab.com/profile/personal_access_tokens). Create a personal token with **api** scope.
10. In Reliza Hub, open your previously-created **Backend Deployment** project.
    - Click on the **Tool (Project settings) icon** to expand settings. 
    - Where it says **CI Integration** and **CI Provider** set radio button to GitLab CI.
    - Set GitLab Schedule Id to the integer obtained above (**12345** for this tutorial).
    - Set GitLab Authentication token to the value of token you just created on GitLab with api scope.
    - Check **Trigger GitLab CI Schedule On Approval**.
    - Set **CI Repository** to your GitLab deployment repository (essentially, your fork of this repository).
    
    *Note:* that if you have more than one project whose docker image is referenced in this deployment repository, this step must be performed for every project like this.
11. Create environment variables in GitLab CI/CD for the project named **INDIRECTORY** and set it to the name of the directory containing your helm chart and specifically values files. For **this** repository this value should be set to **helmchart**.
12. Create environment variables in GitLab CI/CD for the project named **BUILD_ENV_PARAMS**. This environment variable would essentially contain mapping between your values files and environments. Sample value of **BUILD_ENV_PARAMS** would be *--env TEST|k8s_test|values_test.yaml,--env PRODUCTION|k8s_production|values_prod.yaml*. See the section below for more details how this is set up.
13. Create environment variables in GitLab CI **AUTO_COMMITTER_NAME** and **AUTO_COMMITTER_EMAIL**. You could set those to arbitrary values - they would show up for git commits to your deployment project updating definitions.
13. You should end up with the following environment variables in your GitLab CI/CD (9 in total):
    - RELIZA_PROJECT_API_ID
    - RELIZA_PROJECT_API_KEY
    - RELIZA_ORGANIZATION_API_ID
    - RELIZA_ORGANIZATION_API_KEY
    - GIT_PUSH_TOKEN
    - INDIRECTORY
    - BUILD_ENV_PARAMS
    - AUTO_COMMITTER_NAME
    - AUTO_COMMITTER_EMAIL

## Setting up and parsing BUILD_ENV_PARAMS
As mentioned above, **BUILD_ENV_PARAMS** environment variable sets the mapping between your environments, your values and directories where final definitions would reside.
Different environment mappings are comma separated. Each environment mapping contains 3 parts which are single part separated.
First part defines environment, i.e. *--env TEST*. Second part defines output folder, in which templated helm definitions with all substitutions would be saved - and ready for pickup by GitOps tool (such as ArgoCD). Third part is values file which must be used for this environment.

Note that you need to point your specific ArgoCD app installation to the output directory - 2nd parameter per your environment in **BUILD_ENV_PARAMS**.

## Setting flexible project definitions in your values file
Reliza Hub is responsible for selection of good builds that should go into your environments. For this to work, image part of your values files in helm should be templated as following:
**<%PROJECT__51be006c-08f1-4323-a42b-fd6649d5953c__master%>**

Here UUID part - is UUID of specific project, which can be found in Reliza Hub UI when opening project settings (Tool icon) for the desired project. Master is the branch that you want to use for selecting releases.

Following this, Reliza Hub would automatically select releases and do substitutions based on approvals.

You can see the sample in test and prod values files of this project.

## Configuring approval matrix on Reliza Hub

In Reliza Hub navigate to Settings and set approval matrix per environment as desired on the bottom of the page. Note that only Organization Administrator has permissions to do so.

Releases would be selected per environments based on approvals.
